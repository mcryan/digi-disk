<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the table
        Schema::create('disks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('license_number');
            $table->string('vehicle_registration_number');
            $table->string('vin');
            $table->string('gvm');
            $table->string('status');
            $table->date('expiration_date');
            $table->timestamps();
        });

        // Seed it with some sample disks
        $date = new DateTime();
        for ($i = 0; $i <= 20; $i++) {
            DB::table('disks')->insert([
                'license_number' => rand(100000000, 999999999),
                'vehicle_registration_number' => strtoupper(str_random(3) . rand(100, 999) . 'GP'),
                'vin' => strtoupper(str_random(17)),
                'gvm' => 1 . rand(0, 999) . ' KG',
                'status' => 'valid',
                'expiration_date' => $date->format('Y-m-t'),
                'created_at' => $date->format('Y-m-d'),
                'updated_at' => $date->format('Y-m-d')
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disks');
    }
}
