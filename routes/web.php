<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {

    $disks = \App\Disks::all();

    return view('home', [
        'disks' => $disks
    ]);
});

Route::post('save', function (Request $request) {

    $id = $request->input('id');
    $expiration_date = $request->input('expiration_date');
    $status = $request->input('status');

    $disk = \App\Disks::find($id);
    if ($disk) {
        $disk->expiration_date = $expiration_date;
        $disk->status = $status;
        $disk->save();
    }
});

Route::get('/check-license/{id}', function ($id) {

    $disk = \App\Disks::find($id);
    if ($disk) {
        $disk->success = true;
        return response()->json($disk);
    } else {
        return response()->json([
            'success' => false,
            'message' => 'Unable to find a license with an id of `' . $id . '`'
        ]);
    }
});
